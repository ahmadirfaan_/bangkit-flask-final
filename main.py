from flask import Flask, request
# from flask_cors import CORS, cross_origin
from tensorflow.keras.models import load_model
import json

from predict import predict

app = Flask(__name__)
# CORS(app, resources=r'/*')

model = load_model("model_v2.hdf5")

@app.route('/')
def index():
    return "Hi :*"

@app.route('/predict', methods=['GET','POST'])
def predict_images():

    data = request.files.get("file")
    if data == None:
        return 'Got Nothing'
    else:
        prediction = predict(data, model)

    return json.dumps(prediction)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
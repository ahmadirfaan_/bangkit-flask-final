from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.models import load_model
import numpy as np
from tensorflow.keras.preprocessing import image
from PIL import Image
import base64
from io import BytesIO

def preprocess(img):
#   fh = open("temp.png", "wb")
#   fh.write(base64.decodebytes(bytes(img_string, "utf-8")))
#   fh.close()

  
  img = img.resize((299, 299))
  img = image.img_to_array(img)                    
  img = np.expand_dims(img, axis=0)         
  img = preprocess_input(img) 
  return img.reshape((1, 299, 299, 3))

def predict(data, model):
    # img = Image.open(data)
    # buffer = BytesIO()
    # img.save(buffer,format="JPEG")                  #Enregistre l'image dans le buffer
    # myimage = buffer.getvalue()                     
    # b64 = base64.b64encode(myimage).decode('utf-8')
    img = Image.open(data)
    # img = image.load_img(data, target_size=(299, 299))
    img = preprocess(img)
    food_list = ['apple_pie', 'baby_back_ribs', 'baklava', 'beef_carpaccio', 'beef_tartare', 'beet_salad', 'beignets', 'bibimbap', 'bread_pudding', 'breakfast_burrito', 'bruschetta', 'caesar_salad', 'cannoli', 'caprese_salad', 'carrot_cake', 'ceviche', 'cheese_plate', 'cheesecake', 'chicken_curry', 'chicken_quesadilla', 'chicken_wings', 'chocolate_cake', 'chocolate_mousse', 'churros', 'clam_chowder', 'club_sandwich', 'crab_cakes', 'creme_brulee', 'croque_madame', 'cup_cakes', 'deviled_eggs', 'donuts', 'dumplings', 'edamame', 'eggs_benedict', 'escargots', 'falafel', 'filet_mignon', 'fish_and_chips', 'foie_gras', 'french_fries', 'french_onion_soup', 'french_toast', 'fried_calamari', 'fried_rice', 'frozen_yogurt', 'garlic_bread', 'gnocchi', 'greek_salad', 'grilled_cheese_sandwich', 'grilled_salmon', 'guacamole', 'gyoza', 'hamburger', 'hot_and_sour_soup', 'hot_dog', 'huevos_rancheros', 'hummus', 'ice_cream', 'lasagna', 'lobster_bisque', 'lobster_roll_sandwich', 'macaroni_and_cheese', 'macarons', 'miso_soup', 'mussels', 'nachos', 'omelette', 'onion_rings', 'oysters', 'pad_thai', 'paella', 'pancakes', 'panna_cotta', 'peking_duck', 'pho', 'pizza', 'pork_chop', 'poutine', 'prime_rib', 'pulled_pork_sandwich', 'ramen', 'ravioli', 'red_velvet_cake', 'risotto', 'samosa', 'sashimi', 'scallops', 'seaweed_salad', 'shrimp_and_grits', 'spaghetti_bolognese', 'spaghetti_carbonara', 'spring_rolls', 'steak', 'strawberry_shortcake', 'sushi', 'tacos', 'takoyaki', 'tiramisu', 'tuna_tartare', 'waffles']
    serving_list = ['slice', 'serving', 'piece', 'serving', 'cup', 'serving', 'serving', 'serving', 'cup', 'serving', 'piece', 'serving', 'serving', 'serving', 'slice', 'serving', 'serving', 'slice', 'cup', 'serving', 'pcs', 'slice', 'slice', 'serving', 'serving', 'serving', 'pcs', 'cup', 'serving', 'cup', 'egg', 'pcs', 'pcs', 'cup', 'serving', 'serving', 'pcs', 'serving', 'serving', 'serving', 'serving', 'cup', 'slice', 'pcs', 'serving', 'cup', 'small slice', 'cup', 'cup', 'pcs', 'serving', 'tablespoon', '6 pcs', 'pcs', 'cup', 'pcs', 'egg', 'tablespoon', 'cup', 'serving', 'cup', 'serving', 'cup', 'pcs', 'bowl', 'cup', 'serving', 'serving', 'serving', 'pcs', 'cup', 'cup', 'pcs', 'cup', 'serving', 'bowl', 'pcs', 'serving', 'serving', 'serving', 'serving', 'serving', 'serving', 'serving', 'serving', 'piece', 'piece', 'each', 'cup', 'serving', 'serving', 'serving', 'piece', 'serving', 'piece', 'roll', 'serving', 'piece', 'serving', 'serving', 'portion']
    calories_list = ['410', '668', '100', '392', '210', '161', '382', '490', '377', '395', '72', '160', '374', '220', '120', '338', '103', '468', '243', '529', '86', '424', '679', '103', '158', '284', '190', '274', '550', '187', '92', '250', '41', '180', '733', '90', '57', '227', '842', '400', '427', '369', '159', '12', '333', '214', '53', '250', '106', '291', '290', '23', '210', '330', '163', '110', '140', '27', '267', '336', '248', '400', '270', '50', '60', '129', '346', '93', '276', '33', '357', '379', '86', '507', '340', '280', '265', '210', '886', '410', '340', '300', '270', '115', '211', '252', '41', '18', '180', '280', '667', '307', '62', '207', '370', '200', '431', '34', '655', '112', '180']
    print("===========shape===========" + str(img.shape))
    pred = model.predict(img)
    index = np.argmax(pred)
    pred_value = food_list[index]
    serving_value = serving_list[index]
    calorie_value = calories_list[index]
    return {"food_name": pred_value, "calorie": calorie_value, "serving": serving_value}
